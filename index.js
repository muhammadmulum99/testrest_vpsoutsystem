import express from "express";
import cors from "cors";
import session from "express-session";
import dotenv from "dotenv";
import db from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";

import http from "http";

const hostname = "localhost";
import TestTabel from "./routes/Test.js";
import TestNotif from "./routes/TestNotif.js";
import TestComment from "./routes/TestComment.js";

try {
  db.authenticate();
  console.log("Database success connected {-_-}");
} catch (error) {
  console.log(error);
}

dotenv.config();

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
  db: db,
});

// !enable dibawah kalau belum ada tabel nya
// (async () => {
//   await db.sync();
// })();
// async () => {
//   await db.sync({ force: true }).then(() => {
//     console.log("Drop and Resync Database with { force: true }");
//     // initial();
//   });
// };

app.use(
  session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
      secure: "auto",
    },
  })
);

app.use(
  cors({
    credentials: true,
    origin: "*",
  })
);
app.use(express.json());
app.use(TestTabel);
app.use(TestComment);
app.use(TestNotif);
app.get("/",(req,res)=>{
  res.send("test")
})

// ! ini juga
// store.sync();
//swager address

// server.listen(process.env.APP_PORT, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

app.listen(process.env.APP_PORT, hostname, () => {
  console.log(
    `Server up and runningn at http://${hostname}:${process.env.APP_PORT}/   {-_-}`
  );
});
