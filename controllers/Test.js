import TestTabel from "../models/Testmodel.js";
import cryptoJs from "crypto-js";
import db from "../config/Database.js";

export const GetTesaja = async (req, res) => {
  try {
    const response = await TestTabel.findAll();
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "hikendev2022"
    ).toString();
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};


export const getTestajaById = async (req, res) => {
  try {
    const response = await TestTabel.findOne({
      where: {
        uuid_test: req.params.id,
      },
    });
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "ahmedhikendev2022"
    ).toString();
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createTestaja = async (req, res) => {
  const {
    test_nama,
    test_jum,
  } = req.body;
  try {
    await TestTabel.create({
      test_nama: test_nama,
      test_jum: test_jum,
    });
    res.status(201).json({ msg: "Create Berhasil" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const UpdateTestaa = async (req, res) => {
  const response = await TestTabel.findOne({
    where: {
      uuid_test: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  const {
    test_nama,
    test_jum,
  } = req.body;

  try {
    await TestTabel.update(
      {
        test_nama: test_nama,
      test_jum: test_jum,
      },
      {
        where: {
          id: response.uuid_test,
        },
      }
    );
    res.status(200).json({ msg: "Updated" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const deleteTestaja = async (req, res) => {
  const response = await TestTabel.findOne({
    where: {
      uuid_test: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  try {
    await TestTabel.destroy({
      where: {
        uuid_test: response.id,
      },
    });
    res.status(200).json({ msg: " Deleted" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};






