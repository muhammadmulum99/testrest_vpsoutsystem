import TestComment from "../models/TestComment.js";
import cryptoJs from "crypto-js";
import db from "../config/Database.js";

export const GetTesComment = async (req, res) => {
  try {
    const response = await TestComment.findAll();
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "hikendev2022"
    ).toString();
    res.status(200).json(encrypt);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};


export const getTesCommentById = async (req, res) => {
  try {
    const response = await TestComment.findOne({
      where: {
        uuid_comment: req.params.id,
      },
    });
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "ahmedhikendev2022"
    ).toString();
    res.status(200).json(encrypt);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createTesComment = async (req, res) => {
  const {
    GuideId,
    NoteId,
    UserAppId,
    Name,
    Comment,
    Active,
    PictureId,
    DateTime,
    ReplyCommentGuideId

  } = req.body;
  try {
    await TestComment.create({
    GuideId: GuideId,
    NoteId :NoteId,
    UserAppId:UserAppId,
    Name:Name,
    Comment:Comment,
    Active:Active,
    PictureId: PictureId,
    DateTime:DateTime,
    ReplyCommentGuideId:ReplyCommentGuideId
    });
    res.status(201).json({ msg: "Create Berhasil" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const UpdateTesComment = async (req, res) => {
  const response = await TestComment.findOne({
    where: {
      uuid_comment: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  const {
    GuideId: GuideId,
    NoteId :NoteId,
    UserAppId:UserAppId,
    Name:Name,
    Comment:Comment,
    Active:Active,
    PictureId: PictureId,
    DateTime:DateTime,
    ReplyCommentGuideId:ReplyCommentGuideId
  } = req.body;

  try {
    await TestComment.update(
      {
        GuideId: GuideId,
        NoteId :NoteId,
        UserAppId:UserAppId,
        Name:Name,
        Comment:Comment,
        Active:Active,
        PictureId: PictureId,
        DateTime:DateTime,
        ReplyCommentGuideId:ReplyCommentGuideId
      },
      {
        where: {
          id: response.uuid_comment,
        },
      }
    );
    res.status(200).json({ msg: "Updated" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const deleteTesComment = async (req, res) => {
  const response = await TestComment.findOne({
    where: {
      uuid_comment: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  try {
    await TestComment.destroy({
      where: {
        uuid_comment: response.id,
      },
    });
    res.status(200).json({ msg: " Deleted" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};






