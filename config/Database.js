import { Sequelize } from "sequelize";

const db = new Sequelize("testing", "postgres", "hikendev", {
  host: "localhost",
  port: 5432,
  dialect: "postgres",
});

// const db = new Sequelize("auth_db", "root", "hikendev", {
//   host: "localhost",
//   dialect: "mysql",
// });

export default db;
