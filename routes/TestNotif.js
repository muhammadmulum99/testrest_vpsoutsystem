import express from "express";
import {
 GetTesNotif,UpdateTesNotif,createTesNotif,deleteTesNotif,getTesNotifById
} from "../controllers/TestNotif.js";


const router = express.Router();

router.get("/testnotif", GetTesNotif);
router.get(
    "/testnotif/:id",
    getTesNotifById
  );
  router.post(
    "/testnotif",
    createTesNotif
  );
  router.patch(
    "/testnotif/:id",
    UpdateTesNotif
  );
  router.delete(
    "/testnotif/:id",
    deleteTesNotif
  );
  

export default router;
