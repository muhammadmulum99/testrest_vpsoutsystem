import express from "express";
import {
 GetTesaja,
 UpdateTestaa,createTestaja,deleteTestaja,getTestajaById
} from "../controllers/Test.js";


const router = express.Router();

router.get("/testaja", GetTesaja);
router.get(
    "/testaja/:id",
    getTestajaById
  );
  router.post(
    "/testaja",
    createTestaja
  );
  router.patch(
    "/testaja/:id",
    UpdateTestaa
  );
  router.delete(
    "/testaja/:id",
    deleteTestaja
  );
  

export default router;
