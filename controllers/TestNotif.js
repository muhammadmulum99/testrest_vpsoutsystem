import TestNotif from "../models/TestNotif.js";
import cryptoJs from "crypto-js";
import db from "../config/Database.js";

export const GetTesNotif = async (req, res) => {
  try {
    const response = await TestNotif.findAll();
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "hikendev2022"
    ).toString();
    res.status(200).json(encrypt);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};


export const getTesNotifById = async (req, res) => {
  try {
    const response = await TestNotif.findOne({
      where: {
        uuid_notif: req.params.id,
      },
    });
    var encrypt = cryptoJs.AES.encrypt(
      JSON.stringify(response),
      "ahmedhikendev2022"
    ).toString();
    res.status(200).json(encrypt);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createTesNotif = async (req, res) => {
  const {
    GuideId,
    fromUserAppId,
    toUserAppId,
    NoteId,
    NotifText,
    DateTime,
    isView

  } = req.body;
  try {
    await TestNotif.create({
        GuideId : GuideId,
        fromUserAppId: fromUserAppId,
        toUserAppId: toUserAppId,
        NoteId :NoteId,
        NotifText :NotifText,
        DateTime: DateTime,
        isView: isView
    });
    res.status(201).json({ msg: "Create Berhasil" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const UpdateTesNotif = async (req, res) => {
  const response = await TestNotif.findOne({
    where: {
      uuid_notif: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  const {
    GuideId,
    fromUserAppId,
    toUserAppId,
    NoteId,
    NotifText,
    DateTime,
    isView
  } = req.body;

  try {
    await TestNotif.update(
      {
        GuideId : GuideId,
        fromUserAppId: fromUserAppId,
        toUserAppId: toUserAppId,
        NoteId :NoteId,
        NotifText :NotifText,
        DateTime: DateTime,
        isView: isView
      },
      {
        where: {
          id: response.uuid_notif,
        },
      }
    );
    res.status(200).json({ msg: "Updated" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const deleteTesNotif = async (req, res) => {
  const response = await TestNotif.findOne({
    where: {
      uuid_notif: req.params.id,
    },
  });
  if (!response)
    return res.status(404).json({ msg: "data tidak ditemukan" });
  try {
    await TestNotif.destroy({
      where: {
        uuid_notif: response.id,
      },
    });
    res.status(200).json({ msg: " Deleted" });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};






