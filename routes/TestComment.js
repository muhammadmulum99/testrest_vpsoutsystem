import express from "express";
import {
 GetTesComment,UpdateTesComment,createTesComment,deleteTesComment,getTesCommentById
} from "../controllers/TestComment.js";


const router = express.Router();

router.get("/testcoment", GetTesComment);
router.get(
    "/testcomment/:id",
    getTesCommentById
  );
  router.post(
    "/testcomment",
    createTesComment
  );
  router.patch(
    "/testcomment/:id",
    UpdateTesComment
  );
  router.delete(
    "/testcomment/:id",
    deleteTesComment
  );
  

export default router;
